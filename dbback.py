import sqlite3
import logging
import random
import string
import chess


TOKENLENGTH = 20
MAXTOKENITER = 100
ADMINNAME = "ADMIN"


def generate_token(tokenlength):
    """Generates token from digits and upper case letters

    Parameters
    ----------
    tokenlength : int
        length of the token
    """
    return ''.join(
                random.SystemRandom().choice(
                    string.ascii_uppercase + string.digits
                    )
                for _ in range(tokenlength)
            )


class WrongUser(Exception):
    """
    An exception that occurs
    if the wrong user tries to make a move
    not in his turn or not in his game.
    """
    pass


class IllegalMove(Exception):
    """
    An exception that occurs
    if user tries to make an illegal move.
    """
    pass


class GameOver(Exception):
    """
    An exception that occurs
    if the user tries to make a move in a game that has ended.
    """
    pass


class ChessGames:
    class Bot:
        """
        Representation of the bot
        """
        @classmethod
        def initialize(cls, cg):
            """
            Initializes bot table in database in ChessGames instance cg.
            """
            c = cg._db.cursor()
            c.execute(
                """CREATE TABLE IF NOT EXISTS Bot (
                    id INTEGER PRIMARY KEY AUTOINCREMENT,
                    token tinytext UNIQUE,
                    name mediumtext
                    );"""
                )

        def __init__(self, botid, token, name, cg=None):
            """Initializes Bot

            Parameters
            ----------
            botid : int
                Id of the bot in db
            token : string
                Bots token
            name : string
                name of the boat
            """
            self._botid = botid
            self._token = token
            self._name = name
            self._cg = cg

        def asdict(self, token=True):
            """
            Representation of the bot as dictionary

            Parameters
            ----------
            token : bool
                Add token to the dict

            Returns
            -------
            bot : dict
                Representation of a bot
            """
            bot = {}
            bot["id"] = self._botid
            if token:
                bot["token"] = self._token
            bot["name"] = self._name
            return bot

        def __eq__(self, another):
            if isinstance(another, ChessGames.Bot):
                return another.id == self.id
            raise ValueError(f"Can't compare user to {type(another)}")

        def __repr__(self):
            return f"Bot {self._botid}, {self._name}({self._token})"

        @property
        def id(self):
            return self._botid

        @property
        def name(self):
            return self._name

        @property
        def token(self):
            return self._token

    class Game:
        """
        Representation of the game
        """
        @classmethod
        def initialize(cls, cg):
            """
            Initializes Game and Move tables in database in ChessGames instance cg.
            """
            c = cg._db.cursor()
            c.execute(
                """CREATE TABLE IF NOT EXISTS Games (
                    id INTEGER PRIMARY KEY AUTOINCREMENT,
                    white int,
                    black int,
                    gameover boolean,
                    claimdraw boolean,
                    FOREIGN KEY (white) REFERENCES Bot (id),
                    FOREIGN KEY (black) REFERENCES Bot (id)
                    );"""
                )

            c.execute(
                """CREATE TABLE IF NOT EXISTS Moves (
                    id INTEGER PRIMARY KEY AUTOINCREMENT,
                    game int,
                    move tinytext,
                    FOREIGN KEY (game) REFERENCES Games (id)
                    );"""
                )

        def __init__(self, gameid, whitebot, blackbot, gameover, claimdraw, cg=None):
            """Initializes game

            Parameters
            ----------
            gameid : int
                Id of the game in db
            whitebot : ChessGames.Bot
                White player
            blackbot : ChessGames.Bot
                Black player
            gameover : bool
                Flag if game is over
            """
            self._gameid = gameid
            self._whitebot = whitebot
            self._blackbot = blackbot
            self._gameover = gameover
            self._claimdraw = claimdraw
            self._cg = cg

        def asdict(self):
            game = {}
            game["id"] = self._gameid
            game["whitebot"] = self._whitebot.id
            game["blackbot"] = self._blackbot.id
            game["gameover"] = self._gameover
            game["drawclaimed"] = self._claimdraw
            game["moves"] = list(map(str, self.moves))

            if self.gameover:
                # TODO: Move results to db
                game["result"] = self.board.result(claim_draw=self._claimdraw)
            else:
                if len(game["moves"]) % 2 == 0:
                    game["playersturn"] = self._whitebot.id
                else:
                    game["playersturn"] = self._blackbot.id

            return game

        def __repr__(self):
            return f"Game: {self._gameid}, {self._whitebot.name} vs {self._blackbot.name}"

        @property
        def id(self):
            return self._gameid

        @property
        def whitebot(self):
            return self._whitebot

        @property
        def blackbot(self):
            return self._blackbot

        @property
        def gameover(self):
            return self._gameover

        @gameover.setter
        def gameover(self, go):
            cursor = self._cg._db.cursor()
            cursor.execute(
                    'UPDATE Games SET gameover=? WHERE id=?',
                    (go, self.id)
                )
            self._cg._db.commit()
            self._gameover = go

        @property
        def moves(self):
            return self._cg.get_moves(self)

        @property
        def board(self):
            board = chess.Board()
            for move in self.moves:
                board.push(move)
            return board

        @property
        def draw(self):
            return self._claimdraw

        def make_move(self, user, move, claimdraw=False, giveup=False):
            """
            Parameters
            ----------
            user : ChessGames.Bot
                User that makes a move
            move : chess.Move or str
                Move of a player
                If passed as a string, then must be of algebraic notation.
            claimdraw : bool
                If player wants to claim draw
            giveup : bool (not implemented)
                If player wants to give up

            Raises
            ------
            WrongUser
                If the user is not a player in the game or it is not his turn.
            IllegalMove
                If `move` is illegal in current game.
            GameOver
                If current game is over.
            """
            if user != self.whitebot and user != self.blackbot:
                raise WrongUser("User cannot make move in this game")
            elif self.gameover:
                raise GameOver("The game is over")

            # TODO: Implememt giveup feature
            if giveup:
                self.gameover = True
                return

            board = self.board
            if len(board.move_stack) % 2 == 1 and user == self.whitebot:
                raise WrongUser("User cannot make a move on opponents turn")
            elif len(board.move_stack) % 2 == 0 and user == self.blackbot:
                raise WrongUser("User cannot make a move on opponents turn")

            if not isinstance(move, chess.Move):
                move = chess.Move.from_uci(move)
            if move not in board.legal_moves:
                raise IllegalMove("Move is illegal")

            move = self._cg.create_move(self, str(move))
            board.push(move)

            # Check if the game is over, considering if both players claimed a draw
            if board.is_game_over(claim_draw=(claimdraw and self.draw)):
                self.gameover = True
            self._claimdraw = claimdraw

    def __init__(self, dbfile="sb.sqlite", log_level=logging.INFO):
        """Initializes game engine ad database.

        Parameters
        ----------
        dbfile : string
            sqlite database file
        """
        self.logger = logging.getLogger("bot.ChessEngine")
        self.logger.setLevel(log_level)
        self.logger.info("Initializing database")
        try:
            self._db = sqlite3.connect(dbfile, check_same_thread=False)
            ChessGames.Bot.initialize(self)
            ChessGames.Game.initialize(self)
            self.admin_token = generate_token(3*TOKENLENGTH)
            self.logger.info("Database initialized")
        except Exception:
            self.logger.exception("Exception occured during initialization")

    def __del__(self):
        self.logger.info("Deleting database")
        self._db.close()

    def create_bot(self, name):
        """Creates a new bot named `name`

        Parameters
        ----------
        name : str
            Name of the bot

        Returns
        ------
        ChessGames.Bot
            Created bot
        """
        cursor = self._db.cursor()
        token = generate_token(TOKENLENGTH)

        # Creating a new unique token for the bot
        it = 0
        cursor.execute('SELECT * FROM Bot WHERE token=?', (token,))
        row = cursor.fetchone()
        while row is not None and it < MAXTOKENITER:
            token = generate_token()
            cursor.execute('SELECT * FROM Bot WHERE token=?', (token,))
            row = cursor.fetchone()
            it += 1

        cursor.execute(
            "INSERT INTO Bot(token, name) VALUES (?, ?)",
            (token, name)
        )
        self._db.commit()
        id = cursor.lastrowid
        return self.Bot(id, token, name)

    def create_game(self, whitebot, blackbot, gameover=False, claimdraw=False):
        """Creates a new game between `whitebot` and `blackbot`

        Parameters
        ----------
        whitebot : ChessGames.Bot
            White player
        blackbot : ChessGames.Bot
            Black player
        gameover : bool, optional
            Flag if the game is over
        claimdraw : bool, optional
            Flag if draw was claimed

        Returns
        ------
        ChessGames.Game
            Created game

        Raises
        ------
        ValueError
            If wrong type of parameter recieved
        WrongUser
            If one of the players is an admin.
        """
        if not (isinstance(whitebot, self.Bot) and isinstance(whitebot, self.Bot)):
            raise ValueError(
                    f"Bots must be both ChessEngine.Bot, not {type(whitebot)}, {type(blackbot)}"
                )
        elif whitebot.id < 0 or blackbot.id < 0:
            raise WrongUser("Admin cannot play game.")

        cursor = self._db.cursor()
        cursor.execute(
            "INSERT INTO Games(white, black, gameover, claimdraw) VALUES (?, ?, ?, ?)",
            (whitebot.id, blackbot.id, gameover, claimdraw)
        )
        self._db.commit()
        id = cursor.lastrowid
        return self.Game(id, whitebot, blackbot, gameover, claimdraw, self)

    def create_move(self, game, move):
        """Creates a new move in the game.

        Doesn't check if the move is possible.

        Parameters
        ----------
        whitebot : ChessGames.Bot
            White player
        blackbot : ChessGames.Bot
            Black player
        gameover : bool, optional
            Flag if the game is over
        claimdraw : bool, optional
            Flag if draw was claimed

        Returns
        ------
        chess.Move
            Created move

        Raises
        ------
        ValueError
            If wrong type of parameter recieved
        WrongUser
            If one of the players is an admin.
        """
        cursor = self._db.cursor()
        cursor.execute(
            "INSERT INTO Moves(game, move) VALUES (?, ?)",
            (game.id, move)
        )
        self._db.commit()
        return chess.Move.from_uci(move)

    def get_bots(self):
        """Creates an iterator based on the list of all registered bots."""
        cursor = self._db.cursor()
        cursor.execute('SELECT * FROM Bot')
        for bot in cursor:
            yield self.Bot(*bot)

    def get_bot(self, id=None, token=None):
        """Finds a bot with `id` or `token`.
        None is returned if no one has been found.

        Parameters
        ----------
        id : int
            id of the bot
        token : str
            Token of the bot

        Returns
        ------
        ChessGames.Bot or None
            Bot with `id` or `token`
        """
        cursor = self._db.cursor()
        if id is not None:
            cursor.execute('SELECT * FROM Bot WHERE id=?', (id,))
        elif token is not None:
            cursor.execute('SELECT * FROM Bot WHERE token=?', (token,))
        bot = cursor.fetchone()
        if bot is not None:
            bot = self.Bot(*bot)
        elif token == self.admin_token:
            bot = self.Bot(-1, self.admin_token, ADMINNAME)
        return bot

    def get_games_id(self, user=None):
        """Creates an iterator with id of the games where one of the players is `user`."""
        cursor = self._db.cursor()
        if user and user.name != ADMINNAME:
            cursor.execute('SELECT id FROM Games WHERE white=? OR black=?', (user.id, user.id))
        else:
            cursor.execute('SELECT id FROM Games')
        for gameid in cursor:
            yield gameid[0]

    def get_game(self, id):
        """Finds a game with `id`.
        None is returned if no game has been found.

        Parameters
        ----------
        id : int
            id of the game

        Returns
        ------
        ChessGames.Game or None
            Game with `id`
        """
        cursor = self._db.cursor()
        cursor.execute('SELECT * FROM Games WHERE id=?', (id,))
        game = cursor.fetchone()
        if game is not None:
            id, wid, bid, go, draw = game
            game = self.Game(
                        id,
                        self.get_bot(id=wid),
                        self.get_bot(id=bid),
                        go,
                        draw,
                        self
                    )
        return game

    def get_moves(self, game):
        """Created iterator with moves of the `game`"""
        cursor = self._db.cursor()
        cursor.execute('SELECT move FROM Moves WHERE game=?', (game.id,))
        for move in cursor:
            yield chess.Move.from_uci(move[0])
