bad_request = ({
                "error": {
                    "code": 400,
                    "reason": "Bad request"
                    }
            }, 400)

not_found = ({
                "error": {
                    "code": 404,
                    "reason": "Not found"
                    }
            }, 404)

forbidden = ({
                'error': {
                    "code": 403,
                    "reason": "Access forbidden"
                }
            }, 403)

cant_play = ({
                "error": {
                    "code": 418,
                    "reason": "Players can't play a game"
                    }
                }, 418)

cant_make_move = ({"error": {
                    "code": 425,
                    "reason": "You can't make a move"
                    }
                }, 425)

illegalmove = ({"error": {
                    "code": 451,
                    "reason": "Illegal move"
                    }
                }, 451)


gameover = ({"error": {
                    "code": 409,
                    "reason": "The game is over"
                    }
                }, 409)
