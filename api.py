from os import abort
import chess
import flask
from flask import request, jsonify
from dbback import ChessGames, WrongUser, IllegalMove, GameOver
import json

import errormsg

app = flask.Flask(__name__)
app.config["DEBUG"] = True
chessEng = ChessGames("db/db.sqlite")

app.logger.info(f"admin token: {chessEng.admin_token}")

with open("db/token", "w") as f:
    f.write(chessEng.admin_token)


@app.route('/', methods=['GET'])
def home():
    return "<h1>Chess server</h1>"


@app.route('/games', methods=['GET'])
def games():
    user = None
    if 'token' in request.headers:
        user = chessEng.get_bot(token=request.headers["token"])
        if user is None:
            return errormsg.bad_request
        if user.id < 0:
            user = None

    return jsonify({"games": list(chessEng.get_games_id(user))})


@app.route('/games', methods=['POST'])
def new_game():
    user = None
    if 'token' in request.headers:
        user = chessEng.get_bot(token=request.headers["token"])

    if user is None or user.id >= 0:
        return errormsg.forbidden

    if 'white' not in request.args or 'black' not in request.args:
        return errormsg.bad_request
    white = chessEng.get_bot(id=request.args['white'])
    black = chessEng.get_bot(id=request.args['black'])
    if white is None or black is None:
        return errormsg.not_found

    try:
        game = chessEng.create_game(white, black)
    except WrongUser:
        return errormsg.cant_play

    return jsonify({"game": game.asdict()})


@app.route('/games/<id>', methods=['GET'])
def get_game(id):
    if 'token' in request.headers:
        user = chessEng.get_bot(token=request.headers["token"])
        if user is None:
            return errormsg.bad_request

    game = chessEng.get_game(id)
    if game is None:
        return errormsg.not_found
    return jsonify({"game": game.asdict()})


@app.route('/games/<id>', methods=['POST'])
def game_move(id):
    user = None
    if 'token' in request.headers:
        user = chessEng.get_bot(token=request.headers["token"])
    if user is None:
        return errormsg.bad_request

    game = chessEng.get_game(id)
    if game is None:
        return errormsg.not_found
    if 'move' not in request.args:
        return errormsg.bad_request

    move = request.args['move']
    draw = request.args.get("claimdraw", False)
    try:
        game.make_move(user, move, draw)
    except WrongUser:
        return errormsg.cant_make_move
    except IllegalMove:
        return errormsg.illegalmove
    except GameOver:
        return errormsg.gameover
    return jsonify({"game": game.asdict()})


@app.route('/bots', methods=['GET'])
def get_bots():
    user = None
    if 'token' in request.headers:
        user = chessEng.get_bot(token=request.headers["token"])
        if user is None:
            return errormsg.bad_request

    token = (user is not None) and (user.id < 0)

    return jsonify(
            {
                "bots": list(
                            map(
                                lambda bot: bot.asdict(token),
                                chessEng.get_bots()
                                )
                            )
            }
        )


@app.route('/bots', methods=['POST'])
def create_bot():
    user = None
    if 'token' in request.headers:
        user = chessEng.get_bot(token=request.headers["token"])
    if user is None or user.id >= 0:
        return errormsg.forbidden
    if 'name' not in request.args:
        return errormsg.bad_request

    bot = chessEng.create_bot(request.args['name'])
    return jsonify({"bot": bot.asdict()})


@app.route('/bots/<id>', methods=['GET'])
def get_bot(id):
    user = None
    if 'token' in request.headers:
        user = chessEng.get_bot(token=request.headers["token"])
        if user is None:
            return errormsg.bad_request

    token = (user is not None) and (user.id < 0)

    bot = chessEng.get_bot(id=id)
    if bot is None:
        return errormsg.not_found
    else:
        return jsonify({"bot": bot.asdict(token)})


@app.errorhandler(404)
def err404(e):
    response = e.get_response()
    # replace the body with JSON
    response.data = json.dumps({
        "error": {
            "code": 404,
            "reason": "Not found."
                }
            }
        )
    response.content_type = "application/json"
    return response


@app.errorhandler(405)
def err405(e):
    response = e.get_response()
    # replace the body with JSON
    response.data = json.dumps({
        "error": {
            "code": 405,
            "reason": "Method not allowed."
                }
            }
        )
    response.content_type = "application/json"
    return response


if __name__ == "__main__":
    app.run()
