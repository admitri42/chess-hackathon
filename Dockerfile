FROM python:3.9

RUN mkdir -p /usr/src/app/
RUN mkdir -p /usr/src/app/db/
WORKDIR /usr/src/app/


COPY . /usr/src/app/
RUN pip install --no-cache-dir -r requirements.txt


ENV TZ Europe/Moscow

ENV FLASK_APP api.py

CMD ["flask", "run", "--no-debugger", "--host=0.0.0.0"]
EXPOSE 5000