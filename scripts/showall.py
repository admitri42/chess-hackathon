import argparse
import requests
import tabulate


parser = argparse.ArgumentParser(description='Shows table with bots and games.')
parser.add_argument(
                        'server',
                        nargs='?',
                        metavar='server',
                        type=str,
                        help='Adress of the server',
                        default="http://localhost:5000"
                    )

args = parser.parse_args()
server = args.server

r = requests.get(server + "/bots")

if r.status_code == 200:
    bots = r.json()["bots"]
    bots = sorted(bots, key=lambda x: x['id'])
else:
    raise Exception

r = requests.get(server + "/games")

if r.status_code != 200:
    raise Exception

games = [['id', 'white', 'black', 'n_turns', 'result']]
for id in r.json()['games']:
    r = requests.get(server + f"/games/{id}")
    if r.status_code == 200:
        game = r.json()['game']
        # print(game)
        if 'result' in game:
            if game.get('result') == '1/2-1/2':
                w, b = 1/2, 1/2
            else:
                w, b = map(int, game.get('result').split('-'))
            for bid, p in [(game['whitebot'], w), (game['blackbot'], b)]:
                if 'points' in bots[bid-1]:
                    bots[bid-1]['points'] += p
                else:
                    bots[bid-1]['points'] = p

        game['whitebot'] = bots[game['whitebot']-1]['name']
        game['blackbot'] = bots[game['blackbot']-1]['name']
        games.append([game['id'], game['whitebot'], game['blackbot'], len(game['moves']), game.get('result', '-')])

print('Bots:', tabulate.tabulate(bots, tablefmt='fancy_grid', headers="keys"), sep='\n')
print('Games:', tabulate.tabulate(games, tablefmt='fancy_grid', headers="firstrow"), sep='\n')