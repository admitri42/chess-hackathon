import requests
import pandas as pd
import tabulate

server = "http://localhost:5000/"

parser = argparse.ArgumentParser(description='Shows .')
parser.add_argument('config', metavar='config', type=str,
                    help='JSON file with configuration')

r = requests.get(server + "games")


if r.status_code == 200:
    data = [['id', 'white', 'black', 'n_turns', 'result']]
    for id in r.json()['games']:
        r = requests.get(server + f"games/{id}")
        if r.status_code == 200:
            game = r.json()['game']
            # print(game)
            data.append([game['id'], game['whitebot'], game['blackbot'], len(game['moves']), game.get('result', '-')])
    print(tabulate.tabulate(data, tablefmt='fancy_grid', headers="firstrow"))
else:
    print("Error")