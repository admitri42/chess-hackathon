import requests
import pandas as pd
import tabulate

server = "http://localhost:5000/"

r = requests.get(server + "bots")

if r.status_code == 200:
    bots = r.json()["bots"]
    bots = sorted(bots, key=lambda x: x['id'])
else:
    raise Exception

print(tabulate.tabulate(bots, tablefmt='fancy_grid', headers="keys"))
