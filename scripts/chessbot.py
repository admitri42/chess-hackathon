import argparse
import time
import requests

import chess
import chess.engine

parser = argparse.ArgumentParser(description='Interface between UCI engine and chess server')
parser.add_argument(
                        'server',
                        nargs='?',
                        metavar='server',
                        type=str,
                        help='Adress of the server',
                        default="http://localhost:5000"
                    )
parser.add_argument(
                        'botid',
                        metavar='botid',
                        type=int,
                        help='Id of the bot'
                    )
parser.add_argument(
                        'token',
                        metavar='token',
                        type=str,
                        help='Token to access the server'
                    )
parser.add_argument(
                        'engine',
                        nargs='?',
                        metavar='engine',
                        type=str,
                        help='Path to the chess engine',
                        default="./stockfish"
                    )

args = parser.parse_args()
server = args.server
botid = args.botid

headers = {'token': args.token}
engine = chess.engine.SimpleEngine.popen_uci(args.engine)

board = chess.Board()
gameurl = server + "/games"

errors = 0
try:
    while True:
        board.reset()
        rgames = requests.get(gameurl, headers=headers)
        if rgames.status_code != 200:
            raise RuntimeError(f"Server returned error {rgames.status_code} from {gameurl}")
        gamesid = rgames.json()["games"]
        for gameid in gamesid:
            board.reset()
            r = requests.get(gameurl + f"/{gameid}", headers=headers)
            if r.status_code == 200:
                game = r.json()["game"]
                # print(f"Processing game {game['id']}: {not game['gameover'] and game['playersturn'] == botid}, {type(game['playersturn'])}")
                if not game['gameover'] and game['playersturn'] == botid:
                    print(f"Processing game {game['id']}")
                    moves = map(chess.Move.from_uci, game["moves"])
                    for move in moves:
                        board.push(move)
                    result = engine.play(board, chess.engine.Limit(time=0.5))

                    r = requests.post(
                                gameurl + f"/{gameid}",
                                headers=headers,
                                params={
                                    "move": str(result.move),
                                    "claimdraw": result.draw_offered}
                                )
                    if r.status_code != 200:
                        print(r.url, r.status_code)
                        errors += 1

            else:
                errors += 1
        if errors >= 2 * len(gamesid):
            raise RuntimeError(f"Server returned error {r.status_code} from {r.url}")
        time.sleep(1)
finally:
    engine.quit()
